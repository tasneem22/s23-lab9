# Lab 9 - Software Quality and Reliability

Testing manually [https://github.com/](https://github.com/) using [OWASP CSS (OWASP Cheat Sheet Series)](https://cheatsheetseries.owasp.org/index.html) 

## Testing 1 - Forgot Password

**Link**: [Forgot Password](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)

**Requirement**: Return a consistent message for both existent and non-existent accounts.

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open github                                      | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://github.com/login/)                                                          |
| Click on "Forgot Password?"                          | OK, navigated to [reset password page](https://github.com/password_reset)                                           |
| Entered a random non-existing email                  | OK, displayed message: That address is either invalid, not a verified primary email or is not associated with a personal user account.. |
| Entered an existing email                            | OK, displayed message: Check your email for a link to reset your password. |

**Verdict**: Failed - The message displayed for existing and non-existing users is not the same.


## Testing 2 - Authentication

**Link**: [Authentication](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids)

**Requirement**: Make sure your usernames/user IDs are case-insensitive

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open github.com                                     | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://github.com/login/)                                                          |
| Enter an username in Small letters and password      | OK, successfully signed in                                                                                                 |
| Enter an username in Capital letters and password    | OK, successfully signed in                                                                                                 |
| Enter an username in Camel case and password         | OK, successfully signed in                                                                                                 |

**Verdict**: Passed - Username in different cases allows a user to sign in


## Testing 3 - Cross Side Script

**Link**: [Cross Side Script](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)

**Requirement**: make sure that input fields do not execute malicious content upon insertion.

| Step                                                 | Expected Result      |Actual Result                                                                                                                     |
| ---------------------------------------------------- | -------------|-------------------------------------------------------------------------------------------------------------------------- |
| Attempt to inject a script into a search field                                     | Script is not executed and/or input is sanitized | Search results for:`<script>alert("XSS Attack!")</script> `  |


**Verdict**: Passed 